package cat.itb.geoguesser;

import androidx.lifecycle.ViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GeoViewModel extends ViewModel {
    private QuestionModel [] questionBank = {
            new QuestionModel(R.drawable.brasil, R.string.item1, "Chile","Cuba", "Brasil", "Argentina", "Brasil"),
            new QuestionModel(R.drawable.australia,R.string.item2, "Noruega", "Australia", "Austria", "Nueva Zelanda", "Australia"),
            new QuestionModel(R.drawable.finlandia,R.string.item3, "Suiza", "Estonia", "Suecia", "Finlandia", "Finlandia"),
            new QuestionModel(R.drawable.noruega,R.string.item4, "Bélgica", "Noruega", "Holanda", "Suecia", "Noruega"),
            new QuestionModel(R.drawable.china,R.string.item5, "China", "Vietnam", "Tailandia", "Indonesia", "China"),
            new QuestionModel(R.drawable.nigeria,R.string.item6, "Egipto", "Camerún", "Angola", "Nigeria", "Nigeria"),
            new QuestionModel(R.drawable.peru,R.string.item7, "Perú", "Nicaragua", "Ecuador", "Bolivia","Perú"),
            new QuestionModel(R.drawable.canada,R.string.item8, "Rusia", "Groenlandia", "Canadá", "Alaska", "Canadá"),
            new QuestionModel(R.drawable.marruecos,R.string.item9, "Argelia", "Turquía", "Marruecos", "Túnez", "Marruecos"),
            new QuestionModel(R.drawable.coredelnorte,R.string.item10, "Kazajstán", "Corea del Norte", "Japón", "Corea del Sur", "Corea del Norte")
    };
    int contadorGeoViewModel = 0;
    double score = 0;
    int actualChoice = -1;
    final int TOTAL_QUESTIONS = 10;
    private int actual = 0;
    private int contadorPistas = 0;
    private Integer [] noQuestionRepeat = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private Integer [] noRepeat = {0, 1, 2, 3};

    //Para no repetir preguntas
    public void noRepeatChoices(){
        List<Integer> intListNoRepeatChoices = Arrays.asList(noRepeat);
        Collections.shuffle(intListNoRepeatChoices);
    }

    //Para no repetir opciones de respuesta
    public void noRepeatQuestions(){
        List<Integer> intListNoRepeatQuestions = Arrays.asList(noQuestionRepeat);
        Collections.shuffle(intListNoRepeatQuestions);
    }



    public int getQuestionIMG() {  //Retorna la imagen de la pregunta en curso
        return questionBank[noQuestionRepeat[actual]].getImg();
    }



    public QuestionModel getQuestion(){  //Retorna una pregunta diferente cada vez que se llama al método
        return questionBank[noQuestionRepeat[actual]];
    }



    public int getActualGrow(){  //Aumenta en 1 la variable actual para cada vez lanzar una pregunta distinta
        return actual++;
    }



    public int getActual(){  //Retorna el valor de actual
        return actual;
    }



    public int getTotalQuestions(){ //Retorna el número total de preguntas
        return TOTAL_QUESTIONS;
    }



    public void setActualInit(){ //Inicializa de nuevo a 0 la variable actual
        actual = 0;
    }



    public String getChoiceQuestion(){ //Retorna una opción distinta cada vez que se le llama para que cada botón tenga una opción distinta y aleatoria
        actualChoice++;
        return getQuestion().getOptions(noRepeat[actualChoice]);
    }



    public void setActualChoiceInit(){ //Inicializa la variable actualCohoice para que cada vez el orden de las opciones de respuesta sea distinto
        actualChoice = -1;
    }



    public void scoreUp(){ //Aumenta la puntuacion en 1
        score++;
    }



    public void scoreDown(){ //Disminuye la puntuación en 0.5
        score = score - 0.5;
    }



    public double getScore(){ //retorna la puntuación final
        double aux;
        if (score < 0){
            aux = 0;
        }else{
            aux = score;
        }
        return aux * 10;
    }



    public void scoreInit(){ //Reinicia la puntuación a 0
        score = 0;
    }



    public void setContadorPistasGrow(){ //Aumenta el contador de pistas
        contadorPistas++;
    }



    public int getContadorPistas(){ //Retorna el número de pistas
        return contadorPistas;
    }



    public void setContadorPistasInit(){  //Reinicia el contador de pistas a 0
        contadorPistas = 0;
    }



    public int getContadorGeoViewModel(){
        return contadorGeoViewModel;
    }



    public void setContadorGeoViewModelGrow(){
        contadorGeoViewModel = 1;
    }



    public void contadorGeoViewModelInit(){
        contadorGeoViewModel = 0;
    }


}




