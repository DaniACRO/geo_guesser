package cat.itb.geoguesser;


import java.util.Arrays;

public class QuestionModel {
    private int img;
    private int questionID;
    private String [] options = new String[4];
    private String correcta;

    public QuestionModel(int img, int questionID, String options_1, String options_2, String options_3, String options_4, String correcta) {
        this.img = img;
        this.questionID = questionID;
        this.options[0] = options_1;
        this.options[1] = options_2;
        this.options[2] = options_3;
        this.options[3] = options_4;
        this.correcta = correcta;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getOptions(int pos) {
        return options[pos];
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public String getCorrecta() {
        return correcta;
    }

    public void setCorrecta(String correcta) {
        this.correcta = correcta;
    }

    @Override
    public String toString() {
        return "QuestionModel{" +
                "questionID=" + questionID +
                ", options=" + Arrays.toString(options) +
                ", correcta='" + correcta + '\'' +
                '}';
    }
}
