package cat.itb.geoguesser;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button a_button;
    private Button b_button;
    private Button c_button;
    private Button d_button;
    private Button next_ntn;
    private Button hint_btn;
    private Button answerBtn;
    private AlertDialog.Builder dialog;
    ProgressBar bar_progress;
    private ImageView flag;
    private ImageView image_imageView;
    private TextView progresoQuestions;
    private CountDownTimer countDown;
    private int dialogTitle;
    private long valorProgressBar;
    private GeoViewModel geoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Inicializar variables de objetos*/
        a_button = findViewById(R.id.A_button);
        b_button = findViewById(R.id.B_button);
        c_button = findViewById(R.id.C_button);
        d_button = findViewById(R.id.D_button);
        next_ntn = findViewById(R.id.next_button);
        hint_btn = findViewById(R.id.pista_button);
        progresoQuestions = findViewById(R.id.questionProgress);
        bar_progress = findViewById(R.id.progressBar);
        image_imageView = findViewById(R.id.titleLogo_textView);
        flag = findViewById(R.id.flag_imageView);

        /*Inicializa los botones con el evento onClickListener*/
        next_ntn.setOnClickListener(this);
        hint_btn.setOnClickListener(this);

        /*Inicializa ViewModel*/
        geoViewModel = new ViewModelProvider(this).get(GeoViewModel.class);

        /*Cuando es 0 inicia la app, cuando es diferente a cero solo llama a unos métodos específicos*/
        if (geoViewModel.getContadorGeoViewModel() == 0){
            geoViewModel.setContadorGeoViewModelGrow();
            realInici();
        }else{
            if (geoViewModel.getContadorPistas() == 3){ //Si no hacía esto cada vez que cambiaba la orientación daba una pista mas  ¿¿WHY??
                hint_btn.setEnabled(false);
            }
            questionProgresMessage();
            enableButtons();
            showQuestion();
            if (savedInstanceState != null){
                tempProgresBar();
                valorProgressBar = savedInstanceState.getLong("progreso");
                countDown.onTick(valorProgressBar);
                countDown.start();
                /*Se supone que estableceria el valor deseado en el countdown según mi lógica
                * pero no hay manera*/
            }
        }
    }


    /*Cada vez que cambia la orientación del teléfono cancela el progrero  del countdown
    * para que no siga en segundo plano y no salte preguntas de 2 en 2 o 3 en 3*/
    @Override
    protected void onDestroy() {
        countDown.cancel();
        super.onDestroy();
    }



    /*Almacena el valor del coutdown para al girar la orientación retomar el countdown
    * en el valor que se había quedado*/
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("progreso", bar_progress.getProgress());
    }


    /*Método de arranque inical*/
    public void realInici(){
        tempProgresBar();
        questionProgresMessage();
        enableButtons();
        geoViewModel.noRepeatChoices();
        geoViewModel.noRepeatQuestions();
        showQuestion();
    }



    /*Muestra siguiente pregunta / bandera*/
    public void showQuestion(){
        if (geoViewModel.getActual() < 10){
            flag.setImageResource(geoViewModel.getQuestionIMG()); //Muestra la imagen de la bandera

            a_button.setText(geoViewModel.getChoiceQuestion());//Muestra las opciones de respuesta en los botones
            b_button.setText(geoViewModel.getChoiceQuestion());
            c_button.setText(geoViewModel.getChoiceQuestion());
            d_button.setText(geoViewModel.getChoiceQuestion());

            geoViewModel.setActualChoiceInit();
        }else{
            disableButtons();
            finalDialog();
        }
    }



    @Override
    public void onClick(View v) {
       switch (v.getId()) {
           case R.id.next_button:
               next();
               break;
           case R.id.pista_button:
               pistas();
               geoViewModel.setContadorPistasGrow();
               break;
        }
    }


    public void disableButtons(){
        a_button.setEnabled(false);
        b_button.setEnabled(false);
        c_button.setEnabled(false);
        d_button.setEnabled(false);
        hint_btn.setEnabled(false);
    }


    public void enableButtons(){
        a_button.setEnabled(true);
        b_button.setEnabled(true);
        c_button.setEnabled(true);
        d_button.setEnabled(true);
        if (geoViewModel.getContadorPistas() < 3){
            hint_btn.setEnabled(true);
        }
    }



    /*Método llamado al pulsar el botón siguiente o al contestar una pregunta*/
    public void next(){
        geoViewModel.getActualGrow();
        countDown.cancel();
        tempProgresBar();
        questionProgresMessage();
        enableButtons();
        showQuestion();
    }



    /*Método que recoge el id del botón pulsado y checkea la respuesta seleccionada*/
    public void checkAnswer(View v){
        countDown.cancel(); //Cancela el countdown del timebar
        answerBtn = findViewById(v.getId());
        String btnText = answerBtn.getText().toString();

        if (btnText.equalsIgnoreCase(geoViewModel.getQuestion().getCorrecta())){
            dialogTitle = R.string.correct_title; //CORRECTO!!!
            geoViewModel.scoreUp(); //Aumenta la puntuación +1
        }else{
            dialogTitle = R.string.fallo_title; //MEEC...
            geoViewModel.scoreDown();//Disminuye la puntuación en -0.5

        }
        resolutionQuestion();
        disableButtons();
    }




    /*Muestra y cuenta las pistas usadas*/
    public void pistas(){
        countDown.cancel();
        dialogTitle = R.string.pista_title;  //¡¡PISTA!! (mas bien solución...)
        resolutionQuestion();
        if (geoViewModel.getActual() < 10){
            if (geoViewModel.getContadorPistas() > 2){//Cuando el contador de pistas es mayor de 2 deshabilita el botón
                hint_btn.setEnabled(false);
            }
        }else{ //Cuando el user quiere empezar de nuevo habilita el botón y inicializa el contador de pistas a 0
            hint_btn.setEnabled(true);
            geoViewModel.setActualChoiceInit();
        }
        disableButtons();
    }




    /*Muestra dialogo al contestar una pregunta*/
    public void resolutionQuestion(){
        countDown.cancel();
        dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setTitle(dialogTitle);
        dialog.setMessage("Respuesta correcta:  " + geoViewModel.getQuestion().getCorrecta());
        dialog.setPositiveButton(R.string.siguiente, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                next();
            }
        });
        dialog.show();
    }




    /*Muestra el dialogo final al acabar el test*/
    public void finalDialog(){
        countDown.cancel();
        dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setTitle("SCORE:   " + geoViewModel.getScore() + "/100");
        dialog.setMessage(R.string.retry);
        dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                geoViewModel.setActualInit();
                geoViewModel.scoreInit();
                geoViewModel.setContadorPistasInit();
                realInici();
            }
        });
        dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finish();
            }
        });
        dialog.show();
    }




    /*Muestra el texto "Pregunta X de Y*/
    public void questionProgresMessage(){
        if (geoViewModel.getActual() < 10){
            progresoQuestions.setText("Pregunta "+ (geoViewModel.getActual() + 1)+ " de " + geoViewModel.getTotalQuestions());
        }
    }



    /*Crea el cuenta regresiba en la progress bar*/
    public void tempProgresBar(){
        countDown = new CountDownTimer(10000, 100) {
            long timebar = 10000;
            @Override
            public void onTick(long millisUntilFinished) {
                bar_progress.setProgress((int) (millisUntilFinished/ 100));
            }

            @Override
            public void onFinish() {
                bar_progress.setProgress((int) timebar);
                next();
            }
        }.start();
    }

}